package com.example.ussdcore.util;

public class RestResponseEnc {

    private Integer code;
    private String message;
    private Object data;
    private Object comentarios;

    public RestResponseEnc(Integer code) {
        super();
        this.code = code;
    }

    public RestResponseEnc(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public RestResponseEnc(Integer code, Object data) {
        this.code = code;
        this.data = data;
    }

    public RestResponseEnc(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public RestResponseEnc(Integer code, String message, Object data, Object comentarios) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.comentarios = comentarios;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getComentarios() {
        return comentarios;
    }

    public void setComentarios(Object comentarios) {
        this.comentarios = comentarios;
    }
}
