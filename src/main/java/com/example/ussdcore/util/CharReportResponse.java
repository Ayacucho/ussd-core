package com.example.ussdcore.util;

import java.util.ArrayList;

/**
 * Created by AITAMH on 8/01/2020.
 */
public class CharReportResponse {

    private ArrayList<Integer> cantidad;
    private ArrayList<String> etiqueta;

    public CharReportResponse() {
    }

    public CharReportResponse(ArrayList<Integer> cantidad, ArrayList<String> etiqueta) {
        this.cantidad = cantidad;
        this.etiqueta = etiqueta;
    }

    public ArrayList<Integer> getCantidad() {
        return cantidad;
    }

    public void setCantidad(ArrayList<Integer> cantidad) {
        this.cantidad = cantidad;
    }

    public ArrayList<String> getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(ArrayList<String> etiqueta) {
        this.etiqueta = etiqueta;
    }
}
