package com.example.ussdcore.exception;

import java.time.LocalDateTime;

public class ExceptionResponse {
	
	private LocalDateTime dateTime;
	private String mensage;
	private String detail;

	public ExceptionResponse() {}

	public ExceptionResponse(LocalDateTime dateTime, String mensage, String detail) {
		super();
		this.dateTime = dateTime;
		this.mensage = mensage;
		this.detail = detail;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	public String getMensage() {
		return mensage;
	}
	public void setMensage(String mensage) {
		this.mensage = mensage;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	

}
