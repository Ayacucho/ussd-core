/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ussdcore.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Felixab22
 */
@Entity
@Table(name = "tt_ussd_service")
public class TtUssdService implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "b_ser_serviceid")
	private Long bSerServiceid;

	@Basic(optional = false)
	@Column(name = "v_ser_name", unique=true)
	private String vSerName;
	
	@Basic(optional = false)
	@Column(name = "v_ser_description", unique=true)
	private String vSerDescription;
	
	@Column(name = "v_ser_marcation", unique=true)	
	private String vSerMarcation;
	
	@Column(name = "t_ser_date")
	private Date tSerDate;

	@Column(name = "b_ser_code")
	private String bSerCode;

	public TtUssdService() {
	}

	public TtUssdService(Long bSerServiceid) {
		this.bSerServiceid = bSerServiceid;
	}

	public TtUssdService(Long bSerServiceid, String vSerName, String vSerDescription, String vSerMarcation, Date tSerDate, String bSerCode) {
		this.bSerServiceid = bSerServiceid;
		this.vSerName = vSerName;
		this.vSerDescription = vSerDescription;
		this.vSerMarcation = vSerMarcation;
		this.tSerDate = tSerDate;
		this.bSerCode = bSerCode;
	}

	public Long getbSerServiceid() {
		return bSerServiceid;
	}

	public void setbSerServiceid(Long bSerServiceid) {
		this.bSerServiceid = bSerServiceid;
	}

	public String getvSerName() {
		return vSerName;
	}

	public void setvSerName(String vSerName) {
		this.vSerName = vSerName;
	}

	public String getvSerDescription() {
		return vSerDescription;
	}

	public void setvSerDescription(String vSerDescription) {
		this.vSerDescription = vSerDescription;
	}

	public String getvSerMarcation() {
		return vSerMarcation;
	}

	public void setvSerMarcation(String vSerMarcation) {
		this.vSerMarcation = vSerMarcation;
	}

	public Date gettSerDate() {
		return tSerDate;
	}

	public void settSerDate(Date tSerDate) {
		this.tSerDate = tSerDate;
	}

	public String getbSerCode() {
		return bSerCode;
	}

	public void setbSerCode(String bSerCode) {
		this.bSerCode = bSerCode;
	}
}
