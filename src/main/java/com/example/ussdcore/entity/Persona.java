package com.example.ussdcore.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.*;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "PERSONA")
@Inheritance( strategy = InheritanceType.JOINED )
public class Persona implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPERSONA")
    private Long idpersona;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRES", nullable = false, length = 100)
    private String nombres;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDOS", nullable = false, length = 100)
    private String apellidos;

    @Column(name = "DNI", nullable = false, length = 20)
    private String dni;

    @Column(name = "TELEFONO", length = 11)
    private int telefono;

    @Column(name = "correo", length = 100)
    private String correo;

    @Column(name = "SEXO", length = 20)
    private String sexo;

    @Column(name = "FNACIMIENTO", length = 20)
    private String fnacimiento;

    @Column(name = "DIRECDOMI", length = 200)
    private String direcdomi;

    @Column(name = "FOTO")
    private  String foto;

    public Persona(Long idpersona) {
        this.idpersona = idpersona;
    }

    public Persona() {
    }

    public Persona(String nombres, String apellidos, String dni, int telefono, String correo, String sexo, String fnacimiento, String direcdomi, String foto) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.dni = dni;
        this.telefono = telefono;
        this.correo = correo;
        this.sexo = sexo;
        this.fnacimiento = fnacimiento;
        this.direcdomi = direcdomi;
        this.foto = foto;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(Long idpersona) {
        this.idpersona = idpersona;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFnacimiento() {
        return fnacimiento;
    }

    public void setFnacimiento(String fnacimiento) {
        this.fnacimiento = fnacimiento;
    }

    public String getDirecdomi() {
        return direcdomi;
    }

    public void setDirecdomi(String direcdomi) {
        this.direcdomi = direcdomi;
    }
}
