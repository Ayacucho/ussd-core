package com.example.ussdcore.service;

import java.util.List;

import com.example.ussdcore.entity.TtUssdService;



public interface ICRUDService {

	TtUssdService SaveServicio(TtUssdService servicio);

	List<TtUssdService> GetListaServicios();

}
