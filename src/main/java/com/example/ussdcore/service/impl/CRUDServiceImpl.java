package com.example.ussdcore.service.impl;

import com.example.ussdcore.dao.TtUssdServiceDao;
import com.example.ussdcore.entity.TtUssdService;
import org.springframework.stereotype.Service;

import com.example.ussdcore.service.ICRUDService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public  class CRUDServiceImpl implements ICRUDService {

	@Autowired
	private TtUssdServiceDao ttUssdServiceDao;

	@Override
	public TtUssdService SaveServicio(TtUssdService servicio) {
		return this.ttUssdServiceDao.save(servicio);

	}

	@Override
	public List<TtUssdService> GetListaServicios() {
		return  this.ttUssdServiceDao.findAll();

	}
	//implementamos la logina de negocio



}
