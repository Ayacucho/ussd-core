package com.example.ussdcore.service.impl;

import com.example.ussdcore.dao.IUsuarioDao;
import com.example.ussdcore.entity.User;
import com.example.ussdcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUsuarioDao usuarioDao;

    @Override
    public User getPersonaByUsuario(String username) {
        System.out.println("ESTOY EN EL METODO SERVICE USER >>> ");
        User user = this.usuarioDao.findPersonaByUsername(username);

        System.out.println("ID EXTRAIDO DEL USUARIO >>>> " + user.getPersona());

        return user;
    }
}
