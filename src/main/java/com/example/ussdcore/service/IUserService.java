package com.example.ussdcore.service;


import com.example.ussdcore.entity.User;

public interface IUserService {

    User getPersonaByUsuario(String username);
}
