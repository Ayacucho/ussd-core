package com.example.ussdcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UssdCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(UssdCoreApplication.class, args);
	}

}
