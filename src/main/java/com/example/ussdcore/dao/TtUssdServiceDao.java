package com.example.ussdcore.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.ussdcore.entity.TtUssdService;

public interface TtUssdServiceDao extends JpaRepository<TtUssdService, Long> {


	@Query("SELECT s FROM TtUssdService s WHERE s.vSerName =?1")
	TtUssdService findAllByVSerName(String data);


	

}
