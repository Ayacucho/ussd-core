package com.example.ussdcore.dao;


import com.example.ussdcore.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by AITAMH on 18/11/2019.
 */
public interface IRoleDao extends JpaRepository<Role, Long> {

    Role findByDescripcionrol(String roleName);

    @Query("SELECT r FROM Role r WHERE r.idrol = :idrol")
    Role findByIdrol(@Param("idrol") Long idrol);

    //Role findByIdrol(Long idrol);
}
