package com.example.ussdcore.controller;


import com.example.ussdcore.entity.TtUssdService;
import com.example.ussdcore.service.ICRUDService;
import com.example.ussdcore.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.http.HttpStatus;
import java.util.List;

@RestController
@RequestMapping("/ussd/service")
public class ServiceController {

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private ICRUDService Iservicios;

    @Autowired
    private static final Log LOGGER = LogFactory.getLog(ServiceController.class);


    @PostMapping("/saveOrUpdateService")
    public RestResponse saveOrUpdateService(@RequestBody String bolsaJson) throws JsonProcessingException {
        this.objectMapper = new ObjectMapper();
        LOGGER.info(" PARAMETROS DE ENTRADA: '"+ bolsaJson + "'" );
        TtUssdService ttUssdService = this.objectMapper.readValue(bolsaJson, TtUssdService.class);

        this.Iservicios.SaveServicio(ttUssdService);
        return new RestResponse(HttpStatus.OK.value(), "Registro guardado correctamente");

    }

    @GetMapping("/listaServicio")
    public RestResponse GetListaCarta() {

        List<TtUssdService> result = this.Iservicios.GetListaServicios();

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
